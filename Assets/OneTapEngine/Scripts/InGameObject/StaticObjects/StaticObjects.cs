﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticObject : InGameObject
{
    private PoolManager _poolManager;
    private int _durability = 0;
    private bool _triggered = false;
    private bool _destroyOnFinish = false;

    private AudioClip _onEnterSound;
    private AudioClip _remainingSound;
    private AudioClip _leavingSound;
    
    private GameObject _causingVFX;
    private GameObject _remainingVFX;
    private GameObject _leavingVFX;

    public AudioClip onEnterSound { get { return _onEnterSound; } set { _onEnterSound = value; } }
    public AudioClip remainingSound { get { return _remainingSound; } set { _remainingSound = value; } }
    public AudioClip leavingSound { get { return _leavingSound; } set { _leavingSound = value; } } 
    
    public GameObject causingVFX  { get { return _causingVFX; } set { _causingVFX = value; } }
    public GameObject remainingVFX  { get { return _remainingVFX; } set { _remainingVFX = value; } }
    public GameObject leavingVFX  { get { return _leavingVFX; } set { _leavingVFX = value; } }

    
    public PoolManager poolManager { get { return _poolManager; } set { _poolManager = value; } }

    public int durability { get { return _durability; } set { _durability = value; } }
    public bool triggered { get { return _triggered; } set { _triggered = value; } }

    public bool destroyOnFinish { get { return _destroyOnFinish; } set { _destroyOnFinish = value; } }


    // Use this for initialization
    public new void Start()
    {
        base.Start();
        this.GetComponent<Collider>().isTrigger = triggered;
    }

    protected void emmitSound(AudioClip clip, float volume)
    {
        AudioEngine.Instance.CommonAudioSource.PlayOneShot(clip, volume);
    }
}
