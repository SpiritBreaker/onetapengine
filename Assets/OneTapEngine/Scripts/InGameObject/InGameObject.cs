﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InGameObject : MonoBehaviour
{

    public bool subsribed = false;

    private void subscribe()
    {
        GameFlowManager.gameOverNotification += gameOver;
        GameFlowManager.gameStartNotification += gameStart;
        GameFlowManager.gamePauseNotification += gamePause;
        GameFlowManager.gameUnpauseNotification += gameUnpause;
        GameFlowManager.gameLoadNotification += gameLoad;
        subsribed = true;
    }

    private void unsubsribe()
    {
        GameFlowManager.gameOverNotification -= gameOver;
        GameFlowManager.gameStartNotification -= gameStart;
        GameFlowManager.gamePauseNotification -= gamePause;
        GameFlowManager.gameUnpauseNotification -= gameUnpause;
        GameFlowManager.gameLoadNotification -= gameLoad;
        subsribed = false;
    }

    protected void Awake()
    {
        if (isActiveAndEnabled)
        {
            subscribe();
        }
    }

    protected void Start()
    {
    }

    protected virtual void  gameOver()
    {
    }
    protected virtual void gameStart()
    {
    }

    protected virtual void gamePause()
    {
        foreach (MonoBehaviour script in gameObject.GetComponents<MonoBehaviour>())
        {
            script.enabled = false;
        }
    }

    protected virtual void gameUnpause()
    {
        foreach (MonoBehaviour script in gameObject.GetComponents<MonoBehaviour>())
        {
            script.enabled = true;
        }
    }
    protected virtual void gameLoad()
    {
    }

    void onDisabled()
    {
        unsubsribe();
    }

    void onActive()
    {
        subscribe();

    }
    void OnDestroy()
    {
        if (subsribed)
        {
            unsubsribe();
        }
    }
}
