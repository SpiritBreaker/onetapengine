﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AudioEngine : Singleton<AudioEngine> {

	public AudioSource ObstacleAudioSource;
	public AudioSource RewardsAudioSource;
	public AudioSource CommonAudioSource;	

}
