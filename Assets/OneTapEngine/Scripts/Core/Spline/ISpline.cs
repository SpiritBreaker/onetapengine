﻿using UnityEngine;

public struct SplinePoint
{
    public Vector3 position;
    public Vector3 tangent;
    public Vector3 normal;
    public Vector3 binormal;
}

public abstract class ISpline 
{
    public float E = 0.01f;
    public double _length = 0F;
    /// <summary>
    /// Get total length of the spline in doubles
    /// </summary>
    /// <returns></returns>
    public abstract double Length();

    /// <summary>
    /// Get total amount of spline points
    /// </summary>
    /// <returns></returns>
    public abstract int AmountOfPoints { get; set; }

    /// <summary>
    /// Get tangnet vector of segment point
    ///   p0    p1      p2     p3
    ///  -o------o------o------o-
    /// </summary>
    /// <param name="t">point position on segment, could be between 0 and 1</param>
    /// <param name="p0">p0 point before segment</param>
    /// <param name="p1">p1 begin of segment</param>
    /// <param name="p2">p2 end of segment</param>
    /// <param name="p3">p3 point after segment</param>
    /// <returns></returns>
    public abstract Vector3 GetTangent(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3);
    /// <summary>
    /// Get world position of spline segment point
    /// </summary>
    /// <param name="t">point position on segment, could be between 0 and 1</param>
    /// <param name="p0">p0 point before segment</param>
    /// <param name="p1">p1 begin of segment</param>
    /// <param name="p2">p2 end of segment</param>
    /// <param name="p3">p3 point after segment</param>
    /// <returns></returns>
    public abstract Vector3 GetWorldPosition(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3);

    /// <summary>
    /// Get SplinePoint
    /// </summary>
    /// <param name="i">knot number</param>
    /// <param name="t">point on segment</param>
    /// <returns></returns>
    public abstract SplinePoint GetPoint(int i, float t);
    /// <summary>
    /// Get SplinePoint 
    /// </summary>
    /// <param name="i">knot number</param>
    /// <param name="t">point on segment</param>
    /// <param name="Binormal">Binormal of the previous step </param>
    /// <returns></returns>
    public abstract SplinePoint GetPoint(int i, float t, Vector3 Binormal);
    /// <summary>
    /// Get knot poisition
    /// </summary>
    /// <param name="n"></param>
    /// <returns></returns>
    public abstract Transform GetKnot(int n);
    /// <summary>
    /// Convert distance to closest SplinePoint
    /// </summary>
    /// <param name="length">distance</param>
    /// <returns></returns>
    public abstract SplinePoint DistanceToPoint(double length);
    /// <summary>
    /// Convert point do distance
    /// </summary>
    /// <param name="i"></param>
    /// <param name="t"></param>
    /// <param name="space"></param>
    /// <returns></returns>
    public abstract double PointToDistance(int i, float t, Space space = Space.Self);
}





