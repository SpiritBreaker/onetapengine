﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class CatmullRomSpline : ISpline
{
    private readonly PathHolder path_holder;

    public bool isLooping = true;


    public List<double> segment_length = new List<double>();

    public List<double> segment_self_distance = new List<double>();
    public List<List<double>> segment_self_child_distance = new List<List<double>>();


    public CatmullRomSpline(PathHolder path_holder)
    {
        if (path_holder == null)
        {
            throw new ArgumentNullException("pathHolder");
        }
        this.path_holder = path_holder;
    }

    public override int AmountOfPoints
    {
        get { return path_holder.Path.Length; }
        set { }
    }

    public override Vector3 GetWorldPosition(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        Vector3 a = 2f * p1;
        Vector3 b = p2 - p0;
        Vector3 c = 2f * p0 - 5f * p1 + 4f * p2 - p3;
        Vector3 d = -p0 + 3f * p1 - 3f * p2 + p3;
        return 0.5f * (a + (b * t) + (c * t * t) + (d * t * t * t));
    }

    public override Vector3 GetTangent(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        return 0.5f * ((-p0 + p2) + 2 * (2 * p0 - 5f * p1 + 4f * p2 - p3) * (t % 1) + 3f * (-p0 + 3f * p1 - 3f * p2 + p3) * Mathf.Pow((t % 1), 2));
    }

    public override Transform GetKnot(int n)
    {
        return path_holder.Path[n];
    }

    public override SplinePoint GetPoint(int i, float t)
    {
        int path_len = path_holder.Path.Length;

        Vector3 p0 = path_holder.Path[(int)MyMath.mod((int)i - 1, path_len)].position;
        Vector3 p1 = path_holder.Path[(int)MyMath.mod((int)i, path_len)].position;
        Vector3 p2 = path_holder.Path[(int)MyMath.mod((int)i + 1, path_len)].position;
        Vector3 p3 = path_holder.Path[(int)MyMath.mod((int)i + 2, path_len)].position;


        SplinePoint point = new SplinePoint();

        point.position = GetWorldPosition(t, p0, p1, p2, p3);
        point.tangent = GetTangent(t, p0, p1, p2, p3).normalized;
        point.normal = Vector3.Cross(point.tangent, new Vector3(0.0f, 1.0f, 0.0f)).normalized;
        point.binormal = Vector3.Cross(point.normal, point.tangent).normalized;

        return point;
    }

    public override SplinePoint GetPoint(int i, float t, Vector3 Binormal)
    {
        int path_len = path_holder.Path.Length;

        Vector3 p0 = path_holder.Path[(int)MyMath.mod((int)i - 1, path_len)].position;
        Vector3 p1 = path_holder.Path[(int)MyMath.mod((int)i, path_len)].position;
        Vector3 p2 = path_holder.Path[(int)MyMath.mod((int)i + 1, path_len)].position;
        Vector3 p3 = path_holder.Path[(int)MyMath.mod((int)i + 2, path_len)].position;


        SplinePoint point = new SplinePoint();

        point.position = GetWorldPosition(t, p0, p1, p2, p3);
        point.tangent = GetTangent(t, p0, p1, p2, p3).normalized;
        point.normal = Vector3.Cross(point.tangent, Binormal).normalized;
        point.binormal = Vector3.Cross(point.normal, point.tangent).normalized;

        return point;
    }


    public override double Length()
    {
        if (_length == 0F)
        {
            Vector3 previous_position = new Vector3(0F, 0F, 0F);
            Vector3 current_position = new Vector3(0F, 0F, 0F);
            float t = 0F;
            float calc_segment_length = 0F;

            for (int i = 0; i < AmountOfPoints; i++)
            {
                Vector3 p0 = this.GetKnot((int)MyMath.mod((int)i - 1, this.AmountOfPoints)).position;
                Vector3 p1 = this.GetKnot((int)MyMath.mod((int)i, this.AmountOfPoints)).position;
                Vector3 p2 = this.GetKnot((int)MyMath.mod((int)i + 1, this.AmountOfPoints)).position;
                Vector3 p3 = this.GetKnot((int)MyMath.mod((int)i + 2, this.AmountOfPoints)).position;

                if (i == 0)
                {
                    previous_position = this.GetWorldPosition(t, p0, p1, p2, p3);
                }

                List<double> ts = new List<double>();

                while (t < 1.0f)
                {
                    t += base.E;
                    current_position = this.GetWorldPosition(t, p0, p1, p2, p3);
                    _length += (current_position - previous_position).magnitude;
                    calc_segment_length += (current_position - previous_position).magnitude;
                    ts.Add(_length);
                    previous_position = current_position;
                }

                segment_self_child_distance.Add(ts);
                segment_self_distance.Add(_length);

                segment_length.Add(calc_segment_length);
                calc_segment_length = 0F;
                t = 0F;
            }
        }
        return _length;
    }

    public override SplinePoint DistanceToPoint(double length)
    {
        length = length % this.Length();
        int p = Mathf.Abs(segment_self_distance.BinarySearch(length));
        if (p != 0)
        {
            p -= 1;
        }
        int t = Mathf.Abs(segment_self_child_distance[p].BinarySearch(length));
        if (t != 0)
        {
            t -= 1;
        }
        return GetPoint(p, (float)t * E);
    }

    public override double PointToDistance(int i, float t, Space space)
    {
        //Bug is here
        return segment_self_child_distance[i][(int)(t / E)];
    }

    // //Display without having to press play
    // void OnDrawGizmos()
    // {
    //     //Draw the Catmull-Rom spline between the points
    //     for (int i = 0; i < path_holder.Path.Length; i++)
    //     {
    //         //Cant draw between the endpoints
    //         //Neither do we need to draw from the second to the last endpoint
    //         //...if we are not making a looping line
    //         if ((i == 0 || i == path_holder.Path.Length - 2 || i == path_holder.Path.Length - 1) && !isLooping)
    //         {
    //             continue;
    //         }

    //         DisplayCatmullRomSpline(i);
    //     }
    // }

    // //Display a spline between 2 points derived with the Catmull-Rom spline algorithm
    // void DisplayCatmullRomSpline(int pos)
    // {
    //     //The 4 points we need to form a spline between p1 and p2
    //     Vector3 p0 = path_holder.Path[ClampListPos(pos - 1)].position;
    //     Vector3 p1 = path_holder.Path[pos].position;
    //     Vector3 p2 = path_holder.Path[ClampListPos(pos + 1)].position;
    //     Vector3 p3 = path_holder.Path[ClampListPos(pos + 2)].position;

    //     //The start position of the line
    //     Vector3 lastPos = p1;

    //     //The spline's resolution
    //     //Make sure it's is adding up to 1, so 0.3 will give a gap, but 0.2 will work
    //     float resolution = 0.2f;

    //     //How many times should we loop?
    //     int loops = Mathf.FloorToInt(1f / resolution);

    //     for (int i = 1; i <= loops; i++)
    //     {
    //         //Which t position are we at?
    //         float t = i * resolution;

    //         //Find the coordinate between the end points with a Catmull-Rom spline
    //         Vector3 newPos = GetWorldPosition(t, p0, p1, p2, p3);

    //         //Draw this line segment
    //         Gizmos.color = new Color(i / 10.0f, i / 10.0f, i / 10.0f);
    //         Gizmos.DrawLine(lastPos, newPos);

    //         //Save this pos so we can draw the next line segment
    //         lastPos = newPos;
    //     }
    // }

    // //Clamp the list positions to allow looping
    // int ClampListPos(int pos)
    // {
    //     if (pos < 0)
    //     {
    //         pos = path_holder.Path.Length - 1;
    //     }

    //     if (pos > path_holder.Path.Length)
    //     {
    //         pos = 1;
    //     }
    //     else if (pos > path_holder.Path.Length - 1)
    //     {
    //         pos = 0;
    //     }

    //     return pos;
    // }
}


