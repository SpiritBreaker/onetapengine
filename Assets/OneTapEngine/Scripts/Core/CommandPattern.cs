﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class BankAccount
{
    public int balance;
    public int overdfraft_limit;

    public BankAccount()
    {
        balance = 0;
        overdfraft_limit = -500;
    }

    public void deposit(int amount)
    {
        balance += amount;
        Debug.Log("deposited " + amount + ", balance now " + balance);
    }

    public void withdraw(int amount)
    {
        if (balance - amount >= overdfraft_limit)
        {
            balance -= amount;
            Debug.Log("withdrew " + amount + ", balance now " + balance);
        }
    }
}
public enum Action { deposit, withdraw };



interface ICommand
{
	void call();
	void undo();
}


class CommandList : List<Command>, ICommand
{
	public void call()
	{
		for (int i = 0; i < this.Count; i++)
		{
			this[i].call();
		}
	}

	public void undo()
	{
		for (int i = this.Count-1; i >=0; i--)
		{
			this[i].undo();
		}
	}
}

class Command : ICommand
{
    BankAccount account;
    int amount;
    Action action;

    public Command(ref BankAccount account, Action action, int amount)
    {
        this.account = account;
        this.amount = amount;
        this.action = action;
    }

    public void call()
    {
        switch (action)
        {
            case Action.deposit:
                account.deposit(amount);
                break;
            case Action.withdraw:
                account.withdraw(amount);
                break;
        }
    }

    public void undo()
    {
        switch (action)
        {
            case Action.withdraw:
                account.deposit(amount);
                break;
            case Action.deposit:
                account.withdraw(amount);
                break;
        }
    }
}



public class CommandPattern : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        BankAccount ba = new BankAccount();
        CommandList commands = new CommandList
        {
            new Command(ref ba, Action.deposit, 100),
            new Command(ref ba, Action.withdraw, 200)
        };


		commands.call();

        Debug.Log(ba.balance);

		Debug.Log("\n");


		commands.undo();


        Debug.Log(ba.balance);

    }

    // Update is called once per frame
    void Update()
    {
    }
}
