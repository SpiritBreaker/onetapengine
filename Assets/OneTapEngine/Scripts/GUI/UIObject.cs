﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class UIObject : MonoBehaviour {

    public void Awake()
    {
        GameFlowManager.gameOverNotification += gameOver;
        GameFlowManager.gameStartNotification += gameStart;
        GameFlowManager.gamePauseNotification += gamePause;
        GameFlowManager.gameUnpauseNotification += gameUnpause;
        GameFlowManager.gameLoadNotification += gameLoad;
    }

    protected virtual void becomeVisible()
    {
    }

    public void Start()
    {
    }

    protected virtual void gameOver()
    {
    }
    protected virtual void gameStart()
    {
    }
    protected virtual void gamePause()
    {
    }
    protected virtual void gameUnpause()
    {
    }
    protected virtual void gameLoad()
    {
    }

	protected virtual IEnumerator LateCall()
	{
        yield return new WaitForEndOfFrame();
        becomeVisible();

	}

    void OnEnable()
    {
        StartCoroutine(LateCall());
    }

    void OnDisable()
    {
        GameFlowManager.gameOverNotification -= gameOver;
        GameFlowManager.gameStartNotification -= gameStart;
        GameFlowManager.gamePauseNotification -= gamePause;
        GameFlowManager.gameUnpauseNotification -= gameUnpause;
        GameFlowManager.gameLoadNotification -= gameLoad;
    }
}
