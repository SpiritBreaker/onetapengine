﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : UIObject {

    #region Singleton
    private static UIManager _instance;
    public static UIManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<UIManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("UIManager");
                    _instance = container.AddComponent<UIManager>();
                }
            }

            return _instance;
        }
    }
    #endregion

    public UIPage mainPage;
    public UIPage AdPage;

    public UIDialogBox 	OkDialog;
	public UIDialogBox	YesNoDialog;
	public UIDialogBox	RewardForAdDialog;
	public UIDialogBox	ProposalAdDialog;
	public UIDialogBox	AdDialog;

    public bool showAd = false;

    public PageNavigation pageNavigation;

    IEnumerator Ad()
    {
        AdPage.gameObject.SetActive(true);
        yield return new WaitForSecondsRealtime(2f);
        AdPage.gameObject.SetActive(false);
        GameFlowManager.Instance.gameUnpause();
    }

    protected override void gamePause()
    {
        Debug.Log("manager showing ad");
        if (showAd)
        {
            UIDialogBox dialogBox = Instantiate(UIManager.Instance.ProposalAdDialog);
            dialogBox.PopUpDialog("Title", "2xCoins\n for Ad reward", OK, Cancel); 
            showAd = false;
        }
    }



    void OK()
    {
        StartCoroutine(Ad());
    }

    void Cancel()
    {
        GameFlowManager.Instance.gameUnpause();
        GameFlowManager.Instance.gameOver();
        //currentPage.gameObject.SetActive(false);
        mainPage.gameObject.SetActive(true);
    }

}
