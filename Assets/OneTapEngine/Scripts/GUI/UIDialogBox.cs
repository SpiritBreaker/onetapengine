﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDialogBox : UIPage {

	private UIPage PageThatCallDialogBox;

	enum DialogType
	{
		OkDialog,
		YesNoDialog,
		RewardForAdDialog,
		ProposalAdDialog,
		Ad
	}

	public Text dialogText;
	public Button Ok;
	public Button Cancel;


	public delegate void dialogAnswer();
	public dialogAnswer okAnswer;
	public dialogAnswer noAnswer;
	
	private bool m_answer;

	public void PopUpDialog(string title, string text, dialogAnswer dialogAnswerOk, dialogAnswer dialogAnswerNo)
	{
		dialogText.text = text;
		okAnswer = dialogAnswerOk;
		noAnswer = dialogAnswerNo;
	}

	public void DismissDialog()
	{
		if (m_answer)
		{
			if(okAnswer != null)
			{
				okAnswer();
			}
		}
		else
		{
			if(noAnswer != null)
			{
				noAnswer();
			}
		}
		
		OnDestroy();
	}



	public void OnPointerDownOK()
	{
		m_answer = true;
		DismissDialog();
	}

	public void OnPointerDownNo()
	{
		m_answer = false;
		DismissDialog();
	}


	void OnDestroy()
	{
		Destroy(this.gameObject);
	}

}
