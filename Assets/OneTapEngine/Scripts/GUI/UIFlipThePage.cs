﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;

public class UIFlipThePage : UIObject
{

    [SerializeField]
    public UIPage page;

    [SerializeField]
    public UIPage nextPage;

    public bool FlipBack;

    // Use this for initialization
    new void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    // //We make late call because UI should be updated last
    // public IEnumerator LateCall()
    // {
    // 	yield return new WaitForEndOfFrame();
    // 	if (FlipBack)
    // 	{
    // 		nextPage = UIManager.Instance.previousPage;
    // 		page = UIManager.Instance.currentPage;
    // 	}

    // 	nextPage.gameObject.SetActive(true);
    // 	page.gameObject.SetActive(false);
    // 	UIManager.Instance.currentPage = nextPage;
    // 	UIManager.Instance.previousPage = page;	
    // }

    // public virtual void OnPointerDown()
    // {		
    // 	StartCoroutine(LateCall());
    // }


    public virtual void OnPointerDown()
    {
        if (FlipBack)
        {
            UIManager.Instance.pageNavigation.navigationCommandsList.Undo();
        }
        else
        {
            UIManager.Instance.pageNavigation.navigationCommandsList.Add
            (
                    new NavigationCommand(ref UIManager.Instance.pageNavigation, page, nextPage, NavigationAction.Next, false)
            );
            Debug.Log("test");
            UIManager.Instance.pageNavigation.navigationCommandsList.Do();
        }
    }


}
