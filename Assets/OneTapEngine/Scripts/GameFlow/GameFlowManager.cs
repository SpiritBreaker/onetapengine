﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameFlowManager : Singleton<GameFlowManager> {

    public delegate void GameStateEvent();
    public static event GameStateEvent gameLoadNotification;
    public static event GameStateEvent gameOverNotification;
    public static event GameStateEvent gamePauseNotification;
    public static event GameStateEvent gameUnpauseNotification;
    public static event GameStateEvent gameStartNotification;
    public static event GameStateEvent gameQuitNotification;

    enum GameStates { IDLE, PAUSE, PLAY };

    GameStates GameState = GameStates.IDLE;


    void Awake()
    {
    }

    void Start()
    {
        gameLoad();
    }

    public void gameLoad()
    {
        if(gameLoadNotification != null)
        {
            gameLoadNotification();
        }
    }

    public void gameQuit()
    {
        if(gameQuitNotification != null)
        {
            gameQuitNotification();      
        }
    }

    public void gameStart()
    {
        if (gameStartNotification != null)
        {
            gameStartNotification();          
        }
    }

    public void gameOver()
    {  
        if ( gameOverNotification!= null)
        {
            gameOverNotification();
        }
    }

    public void gamePause()
    {   
        if (gamePauseNotification!= null)
        {
           gamePauseNotification();
        }
    }

    public void gameUnpause()
    {
        if (gameUnpauseNotification != null)
        {
            gameUnpauseNotification();
        }
    }

    public void gameRestart()
    {
        gameOver();
        gameStart();

    }
	
	// Update is called once per frame
	void Update () 
    {
	}
}
