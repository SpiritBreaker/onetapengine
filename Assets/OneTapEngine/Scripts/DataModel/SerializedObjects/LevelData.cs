﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;


[Serializable]
public class LevelData : SerializableObject
{
    public string splineMeshPrefab;
    public string name;
    public string prefabName;
    public string previewImage;
    public string goldPrice;

    public override void Compare()
    {
    }
}