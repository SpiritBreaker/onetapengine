﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;
using System;
using System.Linq;
using Newtonsoft.Json;


//https://stackoverflow.com/questions/36239705/serialize-and-deserialize-json-and-json-array-in-unity?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

// DataManaget works as simple implementation of Data Base.

public class DataSerializer
{
    // Collect all types that inherits from SerializableObject, 
    // since only they can be serialized and be used in our data architecture
    List<Type> GameDataTypes = new List<Type>(); 

    // This is very simple representation of DataBase
    // key of the dictionary is a string that represent name of SerializableObject
    // example  
    // -- ["Level", List<SerializableObject>>]
    // -- ["PurchaseOffer", List<SerializableObject>>]
    // -- ["Player", List<SerializableObject>>]
    // ... etc
    public readonly Dictionary<string, List<SerializableObject>> storage = new Dictionary<string, List<SerializableObject>>();

    // Here we collect all Classes represented in our .NET project that 
    // inhereted from SerializableObjects
    public void GetAllDataTypes()
    {
        // Get all types in the project
        var types = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.IsClass);

        foreach (Type t in types)
        {
            //Check if this Type inhereted from SerializableObject
            if (t.BaseType.Name == "SerializableObject")
            {
                GameDataTypes.Add(t);
            }
        }
    }

    // Populate each SerializableObject type in our DB dictionary
    public void CreateDictionaryOfEachType()
    {
        foreach (Type type in GameDataTypes)
        {
            // Create a list
            List<SerializableObject> list = new List<SerializableObject>();
            // Put it into our storage
            storage.Add(type.Name, list);
        }
    }

    private SerializableObject JSONStringToObj(string str, SerializableObject obj)
    {
        JsonUtility.FromJsonOverwrite(str, obj);
        return obj;
    }

    public void LoadGameDataFromJson(string type, string path)
    {
        string dataAsJson = File.ReadAllText(path);
        List<object> instance = JsonConvert.DeserializeObject<List<object>>(dataAsJson);

        List<SerializableObject> listObject;
        storage.TryGetValue(type, out listObject);
        for (int i = 0; i < instance.Count; i++)
        {
            object singleInstance = Activator.CreateInstance(Type.GetType(type));
            singleInstance = JSONStringToObj(instance[i].ToString(), singleInstance as SerializableObject);
            listObject.Add(singleInstance as SerializableObject);
        }
    }

    public List<T> QueryGameData <T>()
    {
        List<SerializableObject> listObject;
        List<T> ChildClassList = new List<T>();
        storage.TryGetValue(typeof(T).ToString(), out listObject);
        ChildClassList = listObject.Cast<T>().ToList();
        return ChildClassList;
    }

    // void Start()
    // {
    //     GetAllDataTypes();
    //     CreateDictionaryOfEachType();
        
    //     LoadGameDataFromJson("Player", Application.dataPath + "/Data/Player/Player.json");
    //     List<Player> playerData = QueryGameData<Player>();
    //     Debug.Log(playerData[0].speed);
    // }



    // --------------------------------------------------------------------------------- //
    //                                                                                   //
    //                          Should be depricated after tests!                        //
    //                                                                                   //
    // --------------------------------------------------------------------------------- //


        //LoadGameDataFromJson("Level", Application.dataPath + "/Data/Level/LevelTest.json");

        //List<Level> levelData = QueryGameData<Level>();

        // List<Level> levelData = QueryGameData<Level>();
        // Debug.Log(levelData[1].customTestClass.testString);



        //Debug.Log(levelData[0].customTestClass.testString);

        // List<object> wrapper = new List<object>();
        // //Activator.CreateInstance(wrapper.MakeGenericType(Type.GetType("Level")));


        // for (int i = 0; i < 10; i++)
        // {
        //     Level newlvl = new Level();
        //     newlvl.name = "level_" + i;
        //     newlvl.customTestClass = new CustomTestClass();
        //     newlvl.customTestClass.fuck = "SosiPisu!";
        //     wrapper.Add(newlvl);
        // }

        //Querry ------------------->>>>>>>
        // {
        //     List<Level> ChildClassList = new List<Level>();
        //     storage.TryGetValue("Level", out listObject);
        //     ChildClassList = listObject.Cast<Level>().ToList();
        //     Debug.Log(ChildClassList[0].customTestClass);
        // }

        //List<Level> lvls = GetInstance<Level>();
        //Debug.Log(lvls[0].name);
        //SaveGameDataArray(wrapper);


        // --------------------------------------------------------------------------------- //
    //                                                                                   //
    //                          Should be depricated after tests!                        //
    //                                                                                   //
    // --------------------------------------------------------------------------------- //

    // private SerializableObject LoadGameDataArray(string path, SerializableObject obj)
    // {
    //     string filePath = Path.Combine(Application.streamingAssetsPath, path);
    //     if (File.Exists(filePath))
    //     {
    //         string dataAsJson = File.ReadAllText(filePath);
    //         JsonUtility.FromJsonOverwrite(dataAsJson, obj);
    //         return obj;
    //     }
    //     else
    //     {
    //         Debug.LogError("Cannot load game data!");
    //         return null;
    //     }
    // }

    // void SaveGameDataArray<T>(T myObject)
    // {
    //     string json = JsonConvert.SerializeObject(myObject, Formatting.Indented);
    //     string path = "Assets/" + typeof(T).ToString() + ".json";
    //     StreamWriter writer = new StreamWriter(path, false);
    //     writer.WriteLine(json);
    //     writer.Close();
    // }

}