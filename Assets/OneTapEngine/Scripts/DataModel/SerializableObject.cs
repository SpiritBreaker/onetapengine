﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;


[Serializable]
public abstract class SerializableObject
{
    public abstract void Compare();
}

