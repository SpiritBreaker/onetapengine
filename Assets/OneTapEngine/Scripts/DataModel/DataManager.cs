﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DataManager : Singleton<DataManager>
{

    private readonly DataSerializer dataSerializer = new DataSerializer();
    private readonly DataIO dataIO = new DataIO();

    [Tooltip("Data folder should be in Assets folder")]
    public string DataPath = "/Data";

    // Use this for initialization
    void Awake()
    {

        dataSerializer.GetAllDataTypes();
        dataSerializer.CreateDictionaryOfEachType();

        DirectoryInfo rootDirectory = new DirectoryInfo("Assets" + DataPath);
        dataIO.FileStorageFromDirectory(rootDirectory);
        dataIO.CollectJsonFiles(rootDirectory);

        foreach (KeyValuePair<string, List<FileInfo>> item in dataIO.storage)
        {
            // //Check if class we are trying to serialize reflected in code
            if (!dataSerializer.storage.ContainsKey(item.Key))
            {
                Debug.LogErrorFormat("Failed to read folder '{0}'. Current name dosn't reflected in DataModel", item.Key);
                continue;
            }
            foreach (FileInfo file in item.Value)
            {
                dataSerializer.LoadGameDataFromJson(item.Key, file.ToString());
            }
        }
    }

    public Dictionary<string, List<SerializableObject>> Storage()
    {
        return dataSerializer.storage;
    }


    public List<T> QueryGameData<T>()
    {
        return dataSerializer.QueryGameData<T>();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
