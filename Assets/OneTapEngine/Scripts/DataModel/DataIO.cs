﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DataIO
{
    //This doctionary represent simple filestorage
    public readonly Dictionary<string, List<FileInfo>> storage = new Dictionary<string, List<FileInfo>>();

    public void FileStorageFromDirectory(DirectoryInfo directory)
    {
        DirectoryInfo[] subDirectories = directory.GetDirectories();
        foreach (DirectoryInfo subDirectory in subDirectories)
        {
            storage.Add(subDirectory.Name, new List<FileInfo>());
        }
    }

    public void GetJsonData(DirectoryInfo directory, string className)
    {
        FileInfo[] files = directory.GetFiles("*.json");
        DirectoryInfo[] subDirectories = directory.GetDirectories();

		foreach (FileInfo file in files)
        {
            List<FileInfo> info;
            storage.TryGetValue(className, out info);
            info.Add(file);
        }

        foreach (DirectoryInfo subDirectory in subDirectories)
        {
            GetJsonData(subDirectory, className);
        }
    }

    public void CollectJsonFiles( DirectoryInfo rootDirectory )
    {
		foreach (DirectoryInfo directory in rootDirectory.GetDirectories())
		{
			GetJsonData(directory, directory.Name);	
		}
    }

    #region  Example_of_usage
    void SimpleUsage()
    {
        // Create root folder
        DirectoryInfo rootDirectory = new DirectoryInfo("Assets/Data");
        
        // Form FileStorage from root directory
        FileStorageFromDirectory(rootDirectory);
        // Populate all json files from child folders
        CollectJsonFiles(rootDirectory);

        foreach (KeyValuePair<string, List<FileInfo>> entry in storage)
        {
            Debug.Log(entry.Key + " " +entry.Value.Count);
        } 

    }

    #endregion

}
