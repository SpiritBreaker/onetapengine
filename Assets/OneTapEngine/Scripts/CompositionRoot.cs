﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompositionRoot: InGameObject {

    #region Singleton
    private static CompositionRoot _instance;
    public static CompositionRoot Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<CompositionRoot>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("CompositionRoot");
                    _instance = container.AddComponent<CompositionRoot>();
                }
            }

            return _instance;
        }
    }
    #endregion

    #region CompositionRoot

	[SerializeField]
    public Spline UnitySpline;
    public ISpline spline;


    #endregion


	// Use this for initialization
	new void Awake () {
		spline = UnitySpline.spline;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
