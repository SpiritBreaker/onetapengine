﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class TestRandomness : MonoBehaviour {

	[SerializeField]
	public float yvalue;

	[SerializeField]
	public float time;

	//@x - this is time of the game
	public float function(float x)
	{
		return Mathf.Pow(1 + (x*20f), 0.6f);
	}

	// Use this for initialization
	void Start () {
		if(Input.GetKeyDown(KeyCode.Space))
		{
			Debug.Log("test");
		}
	}
	
	// Update is called once per frame
	void Update () {
		time = Time.fixedTime;
		yvalue = (int)function(time/60F);
	}
}
