﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButton : MonoBehaviour
{

    public virtual void OnPointerDown()
    {
        GameFlowManager.Instance.gameStart();
    }
}
