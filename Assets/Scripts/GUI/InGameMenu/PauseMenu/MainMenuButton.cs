﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuButton : MonoBehaviour {

	public void OnPointerDown()
	{
		UIDialogBox dialogBox = Instantiate(UIManager.Instance.YesNoDialog);
      	dialogBox.PopUpDialog("Title", "Do you want to quick\nall your progress will be lost", OK, Cancel);
	}


    void OK()
    {
		this.GetComponent<UIFlipThePage>().OnPointerDown();
      	GameFlowManager.Instance.gameUnpause();
		GameFlowManager.Instance.gameOver();
    }

    void Cancel()
    {

    }
}
