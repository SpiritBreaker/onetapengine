﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartButton : MonoBehaviour {

	public void OnPointerDown()
	{
		GameFlowManager.Instance.gameUnpause();
		GameFlowManager.Instance.gameRestart();
	}
}
